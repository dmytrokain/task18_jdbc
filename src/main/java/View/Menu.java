package View;

import model.PC;
import model.Product;
import service.PcService;
import service.ProductService;

import java.sql.SQLException;
import java.util.*;

public class Menu {
    private Map<String, String> menu;
    private Map<String, Printable> methods;
    ProductService prodService;
    PcService pcService;
    Scanner scanner;

    public Menu() {
        scanner = new Scanner(System.in);
        methods = new LinkedHashMap<>();
        menu = new LinkedHashMap<>();
        prodService = new ProductService();
        pcService = new PcService();

        menu.put("1", " 1 - Select all from Product");
        menu.put("2", " 2 - Select all from PC");
        menu.put("3", " 3 - Select by");

        methods.put("1", this::showProducts);
        methods.put("2", this::showPC);
        methods.put("3", this::selectBy);

    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for(String key: menu.keySet()) {
            System.out.println(menu.get(key));
        }
    }

    public void show() throws SQLException {
        outputMenu();
        String scan = scanner.nextLine();

        methods.get(scan).print();

    }

    private void selectBy() throws SQLException {
        System.out.println("1 - From PC\n2 - From Products");
        String scan = scanner.nextLine();

        switch (scan) {
            case "1":
                selectByFromPC();
            case "2":
                selectByFromProduct();
        }
    }

    private void selectByFromPC() throws SQLException {         // BUG?
        System.out.println("Write by and value: ");
        String scan = scanner.nextLine();
        String [] sc = scan.split(" ");

        pcService.findBy(sc[0], sc[1])
                .stream()
                .forEach(System.out::println);
    }

    private void selectByFromProduct() throws SQLException {
        System.out.println("Write by and value: ");
        String scan = scanner.nextLine();
        String [] sc = scan.split(" ");

        prodService.findBy(sc[0], sc[1])
                .stream()
                .forEach(System.out::println);
    }

    private void showProducts() throws SQLException {
        List<Product> list = prodService.findAll();
        System.out.print("Type\t\t\t" + "Model\t\t" + "Maker\n");
        list.stream().map(s -> s.getType() + "\t\t\t" + s.getModel() + "\t\t\t" + s.getMaker())
                .forEach(System.out::println);
    }

    private void showPC() throws SQLException {
        List<PC> list = pcService.findAll();
        System.out.println("Code\t\t" + "Model\t\t\t" + "Price");
        list.stream().map(s -> s.getCode() + "\t\t\t" + s.getModel() + "\t\t\t" + s.getPrice())
                .forEach(System.out::println);
    }
}
