package connection;

import com.mysql.fabric.jdbc.FabricMySQLDriver;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Logger;

public class ConnectionManager {

    private static final String url = "jdbc:mysql://localhost:3306/Labor_SQL";
    private static final String user = "root";
    private static final String password = "999jl044k1";
    private static Logger log = Logger.getLogger(ConnectionManager.class.getName());

    public ConnectionManager() {

    }


    public static Connection getConnection() {
        Connection connection = null;

        if(connection == null) {
            try {
                Driver driver = new FabricMySQLDriver();
                DriverManager.registerDriver(driver);

                connection = DriverManager.getConnection(url, user, password);

                if(!connection.isClosed()) {
                    log.info("Connection established");
                }

            } catch (SQLException e) {
                log.warning("Cannot connect to DB" + e);
            }
        }

        return connection;
    }

    public static void main(String[] args) {
        ConnectionManager.getConnection();
    }
}
