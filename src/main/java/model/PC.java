package model;

import java.util.List;

public class PC {

    private int code;
    private double price;
    private String model;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    @Override
    public String toString() {
        return "PC model: " + getModel()
                + "\ncode: " + getCode()
                + "\nprice:" + getPrice();
    }
}
