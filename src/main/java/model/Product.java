package model;

public class Product {

    private String maker;
    private String type;
    private String model;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMaker() {
        return maker;
    }

    public void setMaker(String maker) {
        this.maker = maker;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    @Override
    public String toString() {
        return "Product maker: " + getMaker()
                + "\nmodel: " + getModel()
                + "\ntype: " + getType();
    }
}
