package service;

import DAO.DAOImplementation.PcDaoImpl;
import model.PC;

import java.sql.SQLException;
import java.util.List;

public class PcService {
    PcDaoImpl impl;

    public PcService() {
        impl = new PcDaoImpl();
    }

    public List findBy(String id, String value) throws SQLException {
        return impl.findBy(id, value);
    }

    public List findAll() throws SQLException {
        return impl.findAll();
    }

    public int delete(String id) throws SQLException {
        return impl.delete(id);
    }
}
