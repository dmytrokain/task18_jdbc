package service;

import DAO.DAOImplementation.ProductDaoImpl;
import model.Product;

import java.sql.SQLException;
import java.util.List;

public class ProductService {
    ProductDaoImpl impl;

    public ProductService() {
        impl = new ProductDaoImpl();
    }

    public List findAll() throws SQLException {
        return impl.findAll();
    }

    public List findBy(String by, String value) throws SQLException {
        return impl.findBy(by, value);
    }

    public int insert(Object obj) throws SQLException {
        return impl.create(obj);
    }

    public int remove(String model) throws SQLException {
        return impl.delete(model);
    }

    public int upDate(Object obj) throws SQLException {
        return impl.update(obj);
    }

}
