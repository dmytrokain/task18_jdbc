package DAO.DAOImplementation;

import DAO.ProductDao;
import connection.ConnectionManager;
import model.Product;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProductDaoImpl implements ProductDao {

    private static final String FIND_ALL = "SELECT * FROM Product";
    private static final String FIND_BY_MAKER = "SELECT * FROM Product where maker =?";
    private static final String FIND_BY_MODEL = "SELECT * FROM Product where model =?";
    private static final String INSERT = "INSERT Product (maker, model, type) VALUES (?, ?, ?)";
    private static final String UPDATE = "UPDATE Product SET maker=?, type=? where model=?";
    private static final String DELETE = "DELETE FROM Product where model=?";


    @Override
    public List findBy(String by, String value) throws SQLException {
        List<Product> prodList = new ArrayList<>();
        String statement = by.equals("maker") ? FIND_BY_MAKER : by.equals("model") ? FIND_BY_MODEL : null;
        Connection connection = ConnectionManager.getConnection();

        try (PreparedStatement prepStatement = connection.prepareStatement(statement)) {
            prepStatement.setString(1, value);
            try (ResultSet resultSet = prepStatement.executeQuery()) {
                while (resultSet.next()) {
                    prodList.add(productInitialize(resultSet.getString("maker"),
                            resultSet.getString("model"), resultSet.getString("type")));
                }
            }
        }
        connection.close();

        return prodList;
    }

    @Override
    public List findAll() throws SQLException {
        List<Product> prodList = new ArrayList<>();

        Connection connection = ConnectionManager.getConnection();

        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    prodList.add(productInitialize(resultSet.getString("maker"),
                            resultSet.getString("model"), resultSet.getString("type")));
                }
            }
        }
        connection.close();

        return prodList;
    }

    @Override
    public int create(Object object) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        Product product = (Product) object;

        try(PreparedStatement prepStat = connection.prepareStatement(INSERT)) {
            prepStat.setString(1, product.getMaker());
            prepStat.setString(2, product.getModel());
            prepStat.setString(3, product.getType());
            return prepStat.executeUpdate();
        }
    }

    @Override
    public int update(Object object) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        Product product = (Product) object;

        try(PreparedStatement prepStat = connection.prepareStatement(UPDATE)) {
            prepStat.setString(1, product.getMaker());
            prepStat.setString(2, product.getType());
            prepStat.setString(3, product.getModel());
            return prepStat.executeUpdate();
        }
    }

    @Override
    public int delete(String model) throws SQLException {
        Connection connection = ConnectionManager.getConnection();

        try(PreparedStatement prepStat = connection.prepareStatement(DELETE)) {
            prepStat.setString(1, model);
            return prepStat.executeUpdate();
        }
    }

    private Product productInitialize(String maker, String model, String type) {
        Product product = new Product();
        product.setMaker(maker);
        product.setModel(model);
        product.setType(type);

        return product;
    }

    public static void main(String[] args) throws SQLException {
        ProductDaoImpl imp = new ProductDaoImpl();

        Product product = new Product();
        product.setType("LS");
        product.setMaker("AR");
        product.setModel("1299");

//        System.out.println(imp.delete("1299"));
//        System.out.println(imp.update(product));
//        System.out.println(imp.findBy("maker", "AR"));
//        System.out.println(imp.create(product));
    }
}
