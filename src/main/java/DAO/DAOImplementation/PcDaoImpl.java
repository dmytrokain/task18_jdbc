package DAO.DAOImplementation;

import DAO.PcDao;
import connection.ConnectionManager;
import model.PC;
import model.Product;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PcDaoImpl implements PcDao {               // TO DO! Add other CRUD operations

    private static final String FIND_ALL = "SELECT * FROM PC";
    private static final String FIND_BY_PRICE = "SELECT * FROM PC where price =?";
    private static final String FIND_BY_MODEL = "SELECT * FROM PC where model =?";
    private static final String DELETE = "DELETE FROM PC where model=?";

    @Override
    public List findBy(String by, String value) throws SQLException {
        List<PC> pcList = new ArrayList<>();
        String statement = by.equals("price") ? FIND_BY_PRICE : by.equals("model") ? FIND_BY_MODEL : null;
        Connection connection = ConnectionManager.getConnection();

        try (PreparedStatement prepStatement = connection.prepareStatement(statement)) {
            prepStatement.setString(1, value);
            try (ResultSet resultSet = prepStatement.executeQuery()) {
                while (resultSet.next()) {
                    pcList.add(pcInitializedObject(resultSet.getInt("code"),
                            resultSet.getString("model"), resultSet.getDouble("price")));
                }
            }
        }
        connection.close();

        return pcList;
    }

    @Override
    public int create(Object entity) throws SQLException {
        return 0;
    }

    @Override
    public int update(Object entity) throws SQLException {
        return 0;
    }

    @Override
    public int delete(String model) throws SQLException {
        Connection connection = ConnectionManager.getConnection();

        try(PreparedStatement preparedStatement = connection.prepareStatement(DELETE)) {
            preparedStatement.setString(1, model);
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public List findAll() throws SQLException {
        List<PC> pcList = new ArrayList<>();

        Connection connection = ConnectionManager.getConnection();

        try (Statement statement = connection.createStatement()) {
            try(ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    pcList.add(pcInitializedObject(resultSet.getInt("code"),
                            resultSet.getString("model"), resultSet.getDouble("price")));
                }
            }
        }
        connection.close();

        return pcList;
    }

    private PC pcInitializedObject(int code, String model, Double price) {
        PC pc = new PC();
        pc.setCode(code);
        pc.setModel(model);
        pc.setPrice(price);

        return pc;
    }
}
