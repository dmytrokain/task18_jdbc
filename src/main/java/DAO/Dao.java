package DAO;

import java.sql.SQLException;
import java.util.List;

public interface Dao<T> {
    List findAll() throws SQLException;
    List findBy(String by, String value) throws SQLException;
    int create(T entity) throws SQLException;
    int update(T entity) throws SQLException;
    int delete(String model) throws SQLException;
}
